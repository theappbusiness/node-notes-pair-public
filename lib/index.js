let express = require('express');
let bodyParser = require('body-parser');

let notes = require('./notes.js');

let app = express();
const port = 3000;

app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.send('API is running');
});

app.get('/notes', function (req, res) {
    res
        .status(400)
        .send('Not allowed to enumerate all notes');
});

app.post('/notes', function (req, res) {
    const note = req.body;
    notes.createNote(
        note,
        function (note) {
            res
                .status(200)
                .send(note);
        },
        function () {
            res
                .status(500)
                .send();
        });
});

app.get('/notes/:id', function (req, res) {
    notes.getNote(
        req.params.id,
        req.query.password,
        function (note) {
            res
                .status(200)
                .send(note);
        },
        function (statusCode) {
            res
                .status(statusCode)
                .send();
        });
});

const server = app.listen(port, function () {
    console.log("API listening on port " + port);
});

module.exports = server;