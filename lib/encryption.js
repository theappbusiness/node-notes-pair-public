const CryptoJS = require("crypto-js");

function encrypt(data, key) {
    return CryptoJS.AES.encrypt(JSON.stringify(data), key);
}

function decrypt(encryptedData, key) {
    try {
        const bytes = CryptoJS.AES.decrypt(encryptedData, key);
        const stringBytes = bytes.toString(CryptoJS.enc.Utf8);
        return JSON.parse(stringBytes);
    }
    catch (err) {
        // If we caught something decryption failed
        return null;
    }
}

module.exports = {
    encrypt,
    decrypt
};