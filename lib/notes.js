const fs = require('fs');
const shortid = require('shortid');
const encryption = require('./encryption.js');

const storageFile = "notes.txt";
let notes = {};

notes.createNote = function (note, success, error) {
    // give the note a new random ID
    note.id = shortid.generate();

    // encrypt
    const encryptedNote = encryption.encrypt(note, note.password);

    // serialize line
    const serializedNote = "\n" + note.id + " " + encryptedNote;

    // save to disk
    fs.appendFile(storageFile, serializedNote, function (err) {
        if (err) {
            error(err);
        } else {
            success(note);
        }
    });
};

notes.getNote = function (noteId, password, success, error) {
    // try read the file
    let notesList;
    try {
        notesList = fs.readFileSync(storageFile, 'utf-8')
            .split('\n')
            .map(function (line) {
                var split = line.split(" ");
                return {
                    key: split[0],
                    value: split[1]
                };
            });
    } catch (e) {
        notesList = [];
    }

    console.log("Found " + notesList.length + " notes on disk");

    // try to find the node with this id
    const idNotes = notesList.filter(function (note) {
        return note.key === noteId;
    });

    if (!idNotes) {
        console.log("Didn't find note with id " + noteId);
        error(404);
    }
    else if (idNotes.length === 1) {
        console.log("Found exactly one note with id " + noteId);

        const note = idNotes[0].value;
        const noteObject = encryption.decrypt(note, password);

        if (noteObject === null) {
            console.log("Decryption failed");
            error(403);
        } else {
            console.log("Decryption succeeded");
            success(noteObject);
        }
    } else {
        console.log("Found more than one note with id " + noteId);
        error(500);
    }
};

module.exports = notes;
