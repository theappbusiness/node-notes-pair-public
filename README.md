# K+C BE Tech Test

## Setup

### Git
- Install git - [official website](https://git-scm.com/)
- Clone this repo

```sh
git clone https://bitbucket.org/theappbusiness/node-notes-pair-public.git
```
- Go to the created directory `node-notes-pair-public` and check if the files are present

### NodeJS + NPM

- Install nodejs [official website](https://nodejs.org/en/)
- Confirm node is installed
```sh
node -v
```
- Confirm npm is installed
```sh
npm -v
```

### Dependencies

- Install the project's node dependencies
```sh
npm install
```

------------

## Datastructures

A little warm-up to practice your javascript. A few tests are failing and we need to write the correct functions to make them pass.

Run all the tests with the following command:

```sh
npm run test-datastructures
```

Run a single test case with the following command:

```sh
npm run test-datastructures -- -g '<name of test>'

# example
npm run test-datastructures -- -g 'should return the correct boolean if the number can be located in the array'
```

## API

Uses nodejs. Supports v.12.0 < version < v.16.0

To run server
```sh
npm start
```
To run unit tests
```sh
npm test
```

To run the tests in watch mode (runs tests every time you make a change to the code)
```sh
npm run test-w
```

## The API

By default the application runs on port 3000

### Creating a note
To add a note we make a `POST` to `http://localhost:3000/notes/`. The note contents is sent as JSON:

```JSON
{
    "title": "example",
    "body": "this is a note",
    "password":"foobar"
}
```

This creates a note a returns the id as plain text.

### Getting a note
To retrieve a note we make a `GET` to `http://localhost:3000/notes/{id}`. A password must be supplied under the `password` query parameter.

This returns the note as JSON:

```JSON
{
    "title": "example",
    "body": "this is a note",
    "password": "foobar",
    "id": "qaEjQZHx8"
}
```

## Implementation details

A note is stored on the server as a file, with the id as the name. Using the id as the name allows for quick access. The contents of the file are formatted as JSON to keep the title and body separate, and the JSON is then encrypted using the password provided, to prevent access of the data on the server without knowledge of the password. The password is sent as a query parameter.

If an incorrect password is sent then the decryption will fail - there is no need to store a password or hash anywhere else.

Ids are simply IDs to guarantee uniqueness. A system that incremented for example would have to be more complicated and have to take into account concurrent requests.

## The task

There are currently 3 tests failing, can you make them pass?
