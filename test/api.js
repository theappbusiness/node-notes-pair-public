let notes = require('../lib/notes.js');
let app = require('../lib/index.js');

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

before(async () => {
    chai.use(chaiHttp);
});

after(async () => {
    app.close();
});

describe('API tests', () => {

    it('should be able to access the API root', (done) => {

        chai.request(app)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });

    it('should not be able to enumerate all notes', (done) => {

        chai.request(app)
            .get('/notes')
            .end((err, res) => {
                res.should.have.status(400);
                done();
            });
    });

    it('should not be able to get a non-existing note', (done) => {

        chai.request(app)
            .get('/notes/noteId')
            .query({ password: 'password' })
            .end((err, res) => {
                res.should.have.status(404);
                done();
            });
    });

    it('should be able to get an existing note with correct password', (done) => {

        // Create a new note
        var note = {
            body: "note body",
            title: "node title",
            password: "password"
        };

        chai.request(app)
            .post('/notes')
            .send(note)
            .end((err, res) => {

                // Get the note id
                var noteId = res.body.id;

                // Get it back
                chai.request(app)
                    .get('/notes/' + noteId)
                    .query({ password: 'password' })
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
            });
    });

    it('should not be able to get an existing note with wrong password', (done) => {

        // Create a new note
        var note = {
            body: "note body",
            title: "node title",
            password: "password"
        };

        chai.request(app)
            .post('/notes')
            .send(note)
            .end((err, res) => {

                // Get the note id
                var noteId = res.body.id;

                // Get it back
                chai.request(app)
                    .get('/notes/' + noteId)
                    .query({ password: 'wrongPassword' })
                    .end((err, res) => {
                        res.should.have.status(403);
                        done();
                    });
            });
    });

    it('should be able to create a note', (done) => {

        var note = {
            id: 1,
            body: "note body",
            title: "node title",
            password: "password"
        };

        chai.request(app)
            .post('/notes')
            .send(note)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });

    it('should be able to delete a note with correct password', (done) => {

        // Create a new note
        var note = {
            body: "note body",
            title: "node title",
            password: "password"
        };

        chai.request(app)
            .post('/notes')
            .send(note)
            .end((err, res) => {

                // Get the note id
                var noteId = res.body.id;

                // Delete it
                chai.request(app)
                    .delete('/notes/' + noteId)
                    .query({password: 'password'})
                    .end((err, res) => {
                        res.should.have.status(204);

                        // Get it back
                        chai.request(app)
                            .get('/notes/' + noteId)
                            .query({ password: 'password' })
                            .end((err, res) => {
                                res.should.have.status(404);
                                done();
                            });
                    });
            });
    });
});
