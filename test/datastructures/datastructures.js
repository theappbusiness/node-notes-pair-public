let datastructures = require('../../lib/datastructures/datastructures.js');

let chai = require('chai');
let expect = chai.expect;

describe("Datastructures in Javascript", () => {
  it("should return the correct boolean if the number can be located in the array", () => {
    const arr = [0, 3, 34, 5, 9, 123, 2];
    const numberInArray = 34;
    const numberNotInArray = 77;

    const resultNumberInArray = datastructures.locateNumber(arr, numberInArray);
    const resultNumberNotInArray = datastructures.locateNumber(arr, numberNotInArray);
    expect(resultNumberInArray).to.be.true
    expect(resultNumberNotInArray).to.be.false
  });

  it("should remove even numbers from the array", () => {
    const arr = [1, 7, 8, 2, 4, 5];
    const result = datastructures.removeEvenNumbers(arr);
    expect(result).to.eql([1,7,5])
  });

  it("should add two arrays together in the order provided", () => {
    const arr1 = [0, 2, 4];
    const arr2 = [6, 10, 20];
    const result = datastructures.addTwoArrays(arr1, arr2);
    expect(result).to.equal([0, 2, 4, 6, 10, 20])
  });

  it("should sum all properties from an array of objects", () => {
    const items = [ { item: "pen", quantity: 2}, {item: "pencil", quantity: 3}, {item: "eraser", quantity: 5 } ];
    const result = datastructures.calculateTotal(items, "quantity");
    expect(result).to.equal(10);
  });
});
