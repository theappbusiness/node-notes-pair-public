let encryption = require('../lib/encryption.js');

let chai = require('chai');
let should = chai.should();

describe('Encryption tests', () => {

    it('should be able to encrypt string with valid key', (done) => {

        var data = "data to encrypt";
        var key = "key";

        var encryptedData = encryption.encrypt(data, key);

        encryptedData.should.not.be.null;

        done();
    });

    it('should be able to decrypt string with valid key', (done) => {

        var data = "data to encrypt";
        var key = "key";

        var encryptedData = encryption.encrypt(data, key);
        var decryptedData = encryption.decrypt(encryptedData);

        should.equal(data, decryptedData);

        done();
    });

    it('should not be able to decrypt string with invalid key', (done) => {

        var data = "data to encrypt";
        var key = "key";
        var wrongKey = "wrongKey";

        var encryptedData = encryption.encrypt(data, key);
        var decryptedData = encryption.decrypt(encryptedData, wrongKey);

        should.not.equal(data, decryptedData);

        done();
    });

    it('should get nothing when decrypting string with invalid key', (done) => {

        var data = "data to encrypt";
        var key = "key";
        var wrongKey = "wrongKey";

        var encryptedData = encryption.encrypt(data, key);
        var decryptedData = encryption.decrypt(encryptedData, wrongKey);

        should.equal(null, decryptedData);

        done();
    });
});
