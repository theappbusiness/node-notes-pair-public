#!/usr/bin/env bash
# install tools
echo "Installing wget, build-essential, g++ and git..."
sudo apt-get update
sudo apt-get install -y -qq wget build-essential g++ git

echo "Updating packages..."

# install important node packages
echo "Installing node packages nodemon, pm2, mocha, gulp, bower ..."
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install -y npm

echo "Done!"
echo "Node version: " `node -v`